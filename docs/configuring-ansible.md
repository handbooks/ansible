## Configuring Ansible

Ansible will searched for configuration file in the following [order](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#the-configuration-file) and use the first file found, all others are ignored:

- `ANSIBLE_CONFIG` (environment variable if set)
- `ansible.cfg` (in the current directory)
- `~/.ansible.cfg` (in the home directory)
- `/etc/ansible/ansible.cfg`

The `ansible-config` utility allows users to see all the configuration settings available, their defaults, how to set them and where their current value comes from.

- [Ansible Configuration Setting](https://docs.ansible.com/ansible/latest/reference_appendices/config.html)
