## Vagrant

```
$ vagrant up
$ vagrant status
Current machine states:

ansible-control           running (virtualbox)
ansible-node01            running (virtualbox)
ansible-node02            running (virtualbox)
$ vagrant ssh ansible-control
```

## Installing Ansible

### [Installing Ansible via YUM](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-release-via-dnf-or-yum):

```
$ sudo yum install -y epel-release
$ sudo yum install ansible
```

### [Installing Ansible via Pip](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-pip):

```
$ sudo yum install python-pip
$ sudo pip install --upgrade pip
$ sudo pip install ansible
```

### Installing Ansible via Pip with `virtualenv`:

```
$ sudo pip install virtualenv
$ mkdir ~/ansible
$ cd ~/ansible
$ echo "ansible>=2.6.0" > requirements.txt
$ virtualenv .venv
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

If you created a virtualenv for ansible, you may need to specify running the python binary with `ansible_python_interpreter`

```
[local]
localhost ansible_connection=local ansible_python_interpreter=/home/vagrant/ansible/.venv/bin/python
```
